using System;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;


namespace OpenUnitySolutions.AutomationUtilities.Editor
{
	public class BuildProcessor : IPreprocessBuildWithReport
	{
		// TODO find some convenient way to set config path
		private const string _VersionConfig = "Assets/_src/Configs/Version Config.asset";
		
		
		public int callbackOrder 
			=> 1;
		
		
		public void OnPreprocessBuild(BuildReport report)
		{
			// TODO config, which scripts to run
			SetBundleVersion();
		}


		private void SetBundleVersion()
		{
			var version = AssetDatabase.LoadAssetAtPath<VersionInfo>(_VersionConfig);

			if (version == null)
				throw new Exception("Can't find version config");
			
			PlayerSettings.bundleVersion = version.ToString();
			
			Debug.Log($"Custom build processor set version to {version}");
		}
	}
}
