using UnityEditor;
using UnityEditor.Build;
using UnityEngine;


namespace OpenUnitySolutions.AutomationUtilities.Editor
{
	public static class PlayerDefines
	{
		public static void Set(BuildTargetGroup targetGroup, string define, bool value)
		{
			if (value)
				Add(targetGroup, define);
			else 
				Remove(targetGroup, define);
		}

		
		public static void Add(BuildTargetGroup targetGroup, string define)
		{
			var (target, defines) = GetDefines(targetGroup);

			if (defines.Contains(define))
			{
				Debug.LogWarning($"Selected build target ({target}) already contains <b>{define}</b> <i>Scripting Define Symbol</i>, can't add.");            
				return;
			}

			defines += ";" + define;
			
			PlayerSettings.SetScriptingDefineSymbols(target, defines);
			Debug.Log($"<b>{define}</b> added to <i>Scripting Define Symbols</i> for selected build target ({target}).");
		}


		public static bool IsSet(BuildTargetGroup targetGroup, string define)
		{
			var (target, defines) = GetDefines(targetGroup);
			return defines.Contains(define);
		}


		public static void Remove(BuildTargetGroup targetGroup, string define)
		{
			var (target, defines) = GetDefines(targetGroup);

			if ( !defines.Contains(define))
			{
				Debug.LogWarning($"Selected build target ({target}) does not contain <b>{define}</b> <i>Scripting Define Symbol</i>, can't remove.");            
				return;
			}

			defines = defines
				.Replace(define, "")
				.Replace(";;", ";")
				.Trim(';');
			
			PlayerSettings.SetScriptingDefineSymbols(target, defines);
			Debug.Log($"<b>{define}</b> removed to <i>Scripting Define Symbols</i> for selected build target ({target}).");
		}


		private static (NamedBuildTarget target, string defines) GetDefines(BuildTargetGroup targetGroup)
		{
			var target = NamedBuildTarget.FromBuildTargetGroup(targetGroup);
			string defines = PlayerSettings.GetScriptingDefineSymbols(target);

			return (target, defines);
		}
	}
}
