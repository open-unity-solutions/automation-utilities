using UnityEngine;


namespace OpenUnitySolutions.AutomationUtilities
{
	[CreateAssetMenu(fileName = "version", menuName = "Open Unity Solutions/Automation Utilities/Version")]
	public class VersionInfo : ScriptableObject
	{
		[SerializeField]
		protected Vector3Int _version;
		
		
		public override string ToString()
			=> $"{_version.x}.{_version.y}.{_version.z}";
	}
}
